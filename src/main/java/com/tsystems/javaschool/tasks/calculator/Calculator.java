package com.tsystems.javaschool.tasks.calculator;


import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    private String [] splitStat;
    private int pos=0;

    public Double calculate(String [] splitStat){

        double first = multiply();

        while (pos<splitStat.length){
            String operator = splitStat[pos];
            if (!operator.equals("-") && !operator.equals("+")){
                break;
            } else {
                pos++;
            }

            double second = multiply();

            if(operator.equals("+")){
                first+=second;
            } else if (operator.equals("-")) {
                first-=second;
            }
        }
        return first;
    }

    public Double multiply(){

        double first = factor();
        while (pos<splitStat.length){
            String operator = splitStat[pos];
            if (!operator.equals("*") && !operator.equals("/")){
                break;
            } else {
                pos++;
            }

            double second = factor();

            if(operator.equals("*")){
                first*=second;
            } else if (operator.equals("/")) {
                if (second == 0){
                    throw new IllegalStateException("Error division by 0");
                }
                first/=second;
            }
        }
        return first;
    }

    public Double factor(){
        String next = splitStat[pos];
        Double result;
        if (next.equals("(")) {
            pos++;
            result = calculate(splitStat);
            String closingBracket = null;
            if (pos < splitStat.length) {
                closingBracket = splitStat[pos];
            } else {
                throw new IllegalStateException("Incorrect expression \")\" was entered");
            }
            if (pos < splitStat.length && closingBracket.equals(")")) {
                pos++;
                return result;
            }
            throw new IllegalStateException("Incorrect expression \")\" was entered");
        }
        if ( pos< splitStat.length-1) {
            pos++;
        }
        return Double.parseDouble(next);
    }


    public String[] splitString (String statement){
        String [] split = statement.split("(?<=[-+*/(),])|(?=[-+*/(),])");
        return split;
    }

    public String showResult (Double result){
        DecimalFormatSymbols dFS = new DecimalFormatSymbols();
        dFS.setDecimalSeparator('.');
        DecimalFormat decimalFormat = new DecimalFormat("#.####", dFS);
        return decimalFormat.format(result);
    }

    public void check (String statement){
        if( statement == null ){
            throw new IllegalStateException("String input - null");
        } else if (statement.equals("") ||
                statement.contains("..") ||
                statement.contains("//") ||
                statement.contains("**") ||
                statement.contains("++") ||
                statement.contains("--") ||
                statement.contains(",")){
            throw new IllegalStateException("Entered a repeating character");
        }
    }

    public String evaluate(String statement) {
        try {
            check(statement);
            splitStat = splitString(statement);
            String res = showResult(calculate(splitStat));
            return res;
        } catch (IllegalStateException e){
            System.out.println(e);
            return null;
        }
    }

}



