package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */

    private int [][] pyramid;

    //calculating the number of rows
    public int sizePiramid (List<Integer> inputNumbers){
        int sizeArr = inputNumbers.size();
        for (int i = 1 ; i<inputNumbers.size() ; i++){
            sizeArr-=i;
            if(sizeArr == 0){
                return i;
            } else if (sizeArr < 0 || inputNumbers.contains(null)){
                throw new CannotBuildPyramidException();
            }
        }
        throw new CannotBuildPyramidException();
    }

    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int count = 1;
        int num = 0;//number of elements in a row
        int rows = sizePiramid(inputNumbers);
        int post = rows*2-1;

        pyramid = new int [rows] [post];

        inputNumbers.sort((o1, o2) ->  o1.compareTo(o2));

        for (int i = 0; i < pyramid.length; i++, count++) {
            int mid = rows-1-i;//position of the first element in the row
            for (int j = 0; j < count*2; j+=2, num++) {
                pyramid[i][mid+j] = inputNumbers.get(num);
            }
        }

        return pyramid;
    }


}
