package com.tsystems.javaschool.tasks.pyramid;

public class CannotBuildPyramidException extends RuntimeException {
    public CannotBuildPyramidException() {
        System.out.println("It is impossible to build a pyramid based on this set of numbers");;
    }
}
