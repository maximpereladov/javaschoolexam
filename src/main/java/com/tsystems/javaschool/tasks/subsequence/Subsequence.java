package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {
    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    private boolean key;
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        try {
            key=equal(x,y);
        } catch (NullPointerException e ){
            throw new IllegalArgumentException();
        }

        return key;
    }


    public boolean equal(List x, List y) {
        int j=0;
        if(x.isEmpty() && !y.equals(null)){
            return true;
        }
        for (int i =0; i < y.size(); i++){
            if(x.get(j).equals(y.get(i))){
                j++;
            }
            if(x.size()==j){
                return true;
            }
        }
        return false;
    }

}
